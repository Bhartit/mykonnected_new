<div class="layout-row content-header row-spacing">Home</div>
<!-- Horizontal Menu -->
<div class="layout-row horizontal-menu">
	<a href="{{folderRoute('/overview')}}">
		<div class="row-item arrow_horizontal_menu {{{ (Request::is('*/overview') ? 'selected' : '') }}}" redirect="overview">Overview</div>
	</a>
	<div class="vertical-separator light"></div>
	<a href="{{folderRoute('/todo-list')}}">
		<div class="row-item arrow_horizontal_menu {{{ (Request::is('*/todo-list'	) ? 'selected' : '') }}}">To Dos</div>
	</a>
	<div class="vertical-separator light"></div>
	@if (!Helper::checkPlan())
	<a href="{{folderRoute('/goals-list')}}">
		<div class="row-item arrow_horizontal_menu {{{ (Request::is('*/goals-list') ? 'selected' : '') }}}">Goals</div>
	</a>
	<div class="vertical-separator light"></div>
	@endif
	<a href="{{ folderRoute('/note-list')}}">
		<div class="row-item arrow_horizontal_menu {{ Request::is('*/note-list')? 'selected' : '' }} ">Notes</div>
	</a>
	<div class="vertical-separator light"></div>
	<a href="{{ folderRoute('/file-list')}}">

		<div class="row-item arrow_horizontal_menu {{ Request::is('*/file-list')? 'selected' : '' }}">Files</div>
	</a>
	<div class="vertical-separator light"></div>
	<a href="{{folderRoute('/learnings-list')}}">
		<div class="row-item arrow_horizontal_menu {{{ (Request::is('*/learnings-list') ? 'selected' : '') }}}">Learnings</div>
	</a>
</div>