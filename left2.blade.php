<div class="home-btn layout-row nav-btn {{ Request::is('*/home') || (Request::is('*/dashboard/*') && Request::isMethod('delete')) ? 'active' : '' }}">
    <a href="{{ tenantRoute('/dashboard/home') }}" class="left"><div class="text">HOME</div></a>
</div>
<div class="home-btn layout-row nav-btn {{ Request::is('*/orgDefinition') || Request::is('*/editorgDefinition')? 'active' : '' }}" >
    <a href="javascript:void(0)" class="left">
        <div class="text">Org Definition</div>
    </a>
</div>
<div class="home-btn layout-row nav-btn">
    <a href="javascript:void(0)" class="left">
        <div class="text">Org Settings</div>
    </a>
</div>
<div class="home-btn layout-row nav-btn {{ Request::is('*/dashboard/rolepermission') ? 'active' : null }}">
    <a href="{{ tenantRoute('/dashboard/rolepermission') }}" class="left">
        <div class="text">Role Permissions</div>
    </a>
</div>